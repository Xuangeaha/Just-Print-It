# Change Log

All notable changes to the "divider" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## 1.0.0 2023-04-16

The `title` and `context` button to **print it** finished. (just-print-it.print-it)

Repair bugs (`JPI-4`):

> The snippets for `CoffeeScript`, `VBScript`, `F#`, `Batch`, `Powershell`, `Perl` and `Clojure` could not be inserted.

## 0.3.2-exp 2023-04-15

Add `title` and `context` button to **print it**, but it hasn't been finished yet.

 *(Message: The function is still developing. You may type "print" in the editor and use the snippet temporarily.)*

## 0.3.1 2023-04-05

Add `description` for snippets.

## 0.3.0 2023-03-25

Add language supports for `CoffeeScript`, `VBScript`, `F#`, `Batch`, `Powershell`, `Perl` and `Clojure`.

## 0.2.0 2023-03-24

Remove language supports for `Scala` and `Ada`.

Repair bugs (`JPI-2`, `JPI-3`):

> Unknown language in `contributes.print-divider.language`. Provided value: Scala
> "contributes.print-divider.language" 中包含未知语言。提供的值: Scala

> Unknown language in `contributes.print-divider.language`. Provided value: Ada
> "contributes.print-divider.language" 中包含未知语言。提供的值: Ada

## 0.1.1 2023-03-19

Correct some mistakes in `README.md`.

## 0.1.0 2023-03-18

Add language supports for `Dart`, `Scala`, `Groovy`, `Visual Basic` and `Ada`.

## 0.0.2 2023-03-11

Repair `C#` snippets (`JPI-1`):

> Console.writeline("") -> Console.WriteLine("")

## 0.0.1 2023-03-11

Add language supports for `Python`, `Java`, `C`, `PHP`, `C#`, `JavaScript`, `TypeScript`, `R`, `Go`, `Rust`, `Swift`, `Ruby`, `Objective-C`, `Lua` and `Julia`.
